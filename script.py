# import pandas as pd

# # Read the Excel file
# df = pd.read_excel('./patchesTest.xlsx')

# # Assuming KB numbers are in a column named 'KB'
# kb_numbers = df['KB ID'].tolist()

# # Format and write to inventory.ini
# with open('inventory.ini', 'w') as f:
#     for kb in kb_numbers:
#         f.write(f'kb_number={kb}\n')  # Adjust the format as needed for your inventory.ini

import pandas as pd

# Your updated script to read the Excel file and extract KB numbers
# Assuming you have a dataframe 'df' and KB numbers are in the 'KB' column
# Modify the script according to your actual Excel file structure

# Read the Excel file
df = pd.read_excel('./patchesTest.xlsx')  # specify the column

# Extract KB numbers into a list
kb_numbers = df[ 'KB ID'].tolist()

# Convert list of KB numbers into a comma-separated string
#kb_numbers_string = ','.join(kb_numbers)
#kb_numbers_string = ','.join(map(str, kb_numbers))
kb_numbers_string = ','.join(map(lambda kb: 'KB' + str(kb), kb_numbers))



# Check if inventory.ini exists and create if not, then append the KB numbers
inventory_file = 'inventory.ini'
with open(inventory_file, 'a') as file:
    file.write(f'kb_numbers={kb_numbers_string}\n')
